const { Router } = require('express'); // Se solicita de express su metodo routes
const router = Router (); // Se llama al metodo Router que se combierte en objeto y se almacena en una constante

const Book = require ('../Models/Book');

router.get('/', async (req, res) => {
    const books = await Book.find();
    res.json(books);
});

router.post('/', async (req, res) => {
    const {title, author, isbn} = req.body;
    const imagePath = '/uploads/' + req.file.filename;
    const newBook = new Book({title, author, isbn, imagePath});
    await newBook.save();
    res.json({message: 'Book Saved'});
});

router.delete('/;id', async (req, res) => {
    await Book.findByIdAndDelete(req.params.id);
    res.json({message: 'Book Deleted'});
});

module.exports = router;