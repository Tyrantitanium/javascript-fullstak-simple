const mongoose = require ('mongoose');
//aca abajo usamos el metodo "connect" desde el modulo "mongoose"
//Este metodo recibe como parametro la cadena de conexión

mongoose.connect(process.env.DB_CONNECTION,)
    .then(db => {console.log('DB is connect')})
    .catch(err => console.error(err));
    
/*
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true
})
    .then(db => console.log('DB is connect'))
    .catch(err => console.error(err));
*/