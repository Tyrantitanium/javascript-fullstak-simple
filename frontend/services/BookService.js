class BookService{

    constructor(){
        this.URI = 'http//localhost:3000/api/books';
    }

    //Metodo para obtener los libros guardados
    async getBook(){
        const response = await fetch(this.URI);
        const books = await response.json();
        return books;
    }

    //Método para crear nuevos libros
    async postBook(book){
        const response = await fetch(this.URI, {
            method: 'POST',
            body: book
        });
        const data = await response.json();
    }

    //Método para eliminar libros
    async deleteBook(bookId){
        //Acá se concatena la dirección con el ID
        const response = await fetch(`${this.URI}/${bookID}`,{
            headers: {
                'Content-type': 'application/json'
            },
            method: 'DELETE'
        });
        const data = await response.json();
    }
}

export default BookService;